Bullhorn
By Jeremy J Bristol, US

Image location: https://thenounproject.com/term/bullhorn/1142/
Artist profile: https://thenounproject.com/jeremy.j.bristol/
Artist website: http://bristolwhite.com/

Attribution text to use: Bullhorn by Jeremy J Bristol from the Noun Project.

I paid a royalty free license on it, so I can use it without attribution.